#!/bin/bash
for x in 0 1 2 3 4 5 6 7
do
	make -C ../.. re ALGO=$x
	mkdir -p $x
	../../bin/main -p ../../rho.arma -c ./config.json -m $x/out.mat -d $x/out.df3 -r $x/out.raw > $x/out.txt
done
